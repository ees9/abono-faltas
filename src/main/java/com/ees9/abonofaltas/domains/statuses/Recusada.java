package com.ees9.abonofaltas.domains.statuses;

public class Recusada implements Status {

    @Override
    public Status solicitar() {
        throw new IllegalStateException("A solicitação já está recusada.");
    }

    @Override
    public Status aprovar() {
        throw new IllegalStateException("A solicitação já está recusada.");
    }

    @Override
    public Status recusar() {
        throw new IllegalStateException("A solicitação já está recusada.");
    }

    @Override
    public Status retornar() {
        throw new IllegalStateException("A solicitação já está recusada.");
    }
}
