package com.ees9.abonofaltas.domains.statuses;

public interface Status {

    public Status solicitar();

    public Status aprovar();

    public Status recusar();

    public Status retornar();
}
