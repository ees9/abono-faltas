package com.ees9.abonofaltas.domains.statuses;

public class NovaSolicitacao implements Status {

    @Override
    public Status solicitar() {
        return new AguardandoChefia();
    }
    
    @Override
    public Status aprovar() {
        throw new IllegalStateException("A solicitação ainda não foi emitida.");
    }

    @Override
    public Status recusar() {
        throw new IllegalStateException("A solicitação ainda não foi emitida.");
    }

    @Override
    public Status retornar() {
        throw new IllegalStateException("A solicitação ainda não foi emitida.");
    }
}
