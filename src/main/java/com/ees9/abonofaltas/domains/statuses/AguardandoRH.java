package com.ees9.abonofaltas.domains.statuses;

public class AguardandoRH implements Status {

    @Override
    public Status solicitar() {
        throw new IllegalStateException("A solicitação já foi solicitada e está aguardando RH");
    }

    @Override
    public Status aprovar() {
        return new Aprovada();
    }

    @Override
    public Status recusar() {
        return new Recusada();
    }

    @Override
    public Status retornar() {
        return new AguardandoChefia();
    }
}
