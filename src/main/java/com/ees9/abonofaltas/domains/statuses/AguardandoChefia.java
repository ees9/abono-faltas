package com.ees9.abonofaltas.domains.statuses;

public class AguardandoChefia implements Status {

    @Override
    public Status solicitar() {
        throw new IllegalStateException("A solicitação já foi emitida.");
    }

    @Override
    public Status aprovar() {
        return new AguardandoRH();
    }

    @Override
    public Status recusar() {
        return new Recusada();
    }

    @Override
    public Status retornar() {
        throw new IllegalStateException("A solicitação só pode ser retornada pelo RH.");
    }
}
