package com.ees9.abonofaltas.domains.statuses;

public class Aprovada implements Status {
    

    @Override
    public Status solicitar() {
        throw new IllegalStateException("A solicitação já está aprovada.");
    }

    @Override
    public Status aprovar() {
        throw new IllegalStateException("A solicitação já está aprovada.");
    }

    @Override
    public Status recusar() {
        throw new IllegalStateException("A solicitação já está aprovada.");
    }

    @Override
    public Status retornar() {
        throw new IllegalStateException("A solicitação já está aprovada.");
    }
}
