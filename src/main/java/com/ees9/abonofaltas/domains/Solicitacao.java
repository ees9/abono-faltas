package com.ees9.abonofaltas.domains;

import com.ees9.abonofaltas.domains.statuses.NovaSolicitacao;
import com.ees9.abonofaltas.domains.statuses.Status;

import java.util.Date;

public class Solicitacao {

    private Funcionario funcionario;
    private Date inicio;
    private Date termino;
    private String motivo;
    private String observacao;
    protected Status status;

    public Solicitacao(){
        status = new NovaSolicitacao();
    }
    
    public void solicitar() {
        status = status.solicitar();
    }

    public void aprovar() {
        status = status.aprovar();
    }

    public void recusar() {
        status = status.recusar();
    }

    public void retornar(String motivo) {
        status = status.retornar();
        this.motivo = motivo;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getTermino() {
        return termino;
    }

    public void setTermino(Date termino) {
        this.termino = termino;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Status getStatus() {
        return status;
    }
}
