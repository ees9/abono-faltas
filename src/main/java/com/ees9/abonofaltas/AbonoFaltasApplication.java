package com.ees9.abonofaltas;

import com.ees9.abonofaltas.domains.Funcionario;
import com.ees9.abonofaltas.domains.Solicitacao;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbonoFaltasApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbonoFaltasApplication.class, args);
	}
}
