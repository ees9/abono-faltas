package com.ees9.abonofaltas;

import com.ees9.abonofaltas.domains.Solicitacao;
import com.ees9.abonofaltas.domains.statuses.Aprovada;
import com.ees9.abonofaltas.domains.statuses.NovaSolicitacao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.assertEquals;

@SuppressWarnings("ALL")
@RunWith(SpringRunner.class)
@SpringBootTest
public class AprovadaTests {

    @Test
    public void status_nova_solicitacao_quando_criada() {
        Solicitacao solicitacao = new Solicitacao();
        assertTrue(solicitacao.getStatus() instanceof NovaSolicitacao);
    }
//
//
//    @Test
//    public void quando_aprovar__entao_nao_esta_recusado_nao_esta_solicitado() {
//        Aprovada aprovada = new Aprovada();
//
//        aprovada.aprovar();
//
//        assertTrue(aprovada.getAprovado());
//        assertFalse(aprovada.getRecusado());
//        assertFalse(aprovada.getSolicitado());
//    }
//
//    @Test
//    public void quando_recusar__entao_nao_esta_aprovado_nao_esta_solicitado() {
//        Aprovada aprovada = new Aprovada();
//
//        aprovada.recusar();
//
//        assertTrue(aprovada.getRecusado());
//        assertFalse(aprovada.getAprovado());
//        assertFalse(aprovada.getSolicitado());
//    }
}
